import React, { useState, useEffect } from "react";
import NumberFormat from "react-number-format";
import NavBar from "./../../shared/nav/navBar.js";
import { MOBILES } from "./../../fakedb/mobile.js";
import { useHistory } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlus,
  faMinus,
  faShoppingCart
} from "@fortawesome/free-solid-svg-icons";

import MasterCard from "./../../assets/images/master-card.png";
import Visa from "./../../assets/images/visa.png";

const Summary = props => {
  const history = useHistory();
  const id = props.match.params.id;
  const data = MOBILES[id - 1];
  const { image } = data;
  const [counter, setCounter] = useState(1);
  const [totalPrice, setTotalPrice] = useState(data.price);
  const [tax, setTax] = useState();

  const clickBuy = () => {
    history.push(`/home`);
  };
  const updateCounter = val => {
    if (counter > 0) {
      if (val === -1 && counter === 1) return false;
      let newCounter = counter + val;
      setTotalPrice(data.price * newCounter);
      setCounter(newCounter);
      calTax();
    }
  };
  const calTax = () => {
    const tax = (totalPrice * 7) / 100;
    setTax(tax);
  };
  useEffect(() => {
    calTax();
  });
  return (
    <>
      <NavBar />
      <div className="w-full p-5 md:px-20 px-5 py-10">
        <div className="shadow-xl border-t border-gray-100 rounded-md p-10">
          <div className="border-b border-gray-400 pb-4">
            <div className="font-bold">Shopping Cart</div>
            <div className="text-gray-500 mt-1 text-sm">
              Free delivery and free returns
            </div>
          </div>
          <div className="border-b border-gray-400 pb-6 justify-center md:justify-start flex flex-wrap mt-6">
            <div className="w-40 ">
              <div className="p-4 shadow-lg border-t border-gray-100 rounded-lg">
                <img src={image} className="w-full" alt="ima" />
              </div>
            </div>
            <div className="w-full flex flex-wrap md:w-7/12 pt-0 md:pt-4 p-4">
              <div className="w-full mt-2 flex flex-wrap justify-center">
                <div className="w-full md:w-2/3 text-center md:text-left uppercase font-bold text-xl">
                  {data.name}
                </div>
                <div className="w-full mt-4 sm:mt-0 flex justify-center md:w-1/3 mt-2 lg:-mt-1 ">
                  <div className="flex h-8 fit-content px-4 py-1 rounded-lg bg-gray-200">
                    <div className="w-5">
                      <FontAwesomeIcon
                        className="cursor-pointer text-gray-500"
                        onClick={() => {
                          updateCounter(-1);
                        }}
                        icon={faMinus}
                      />
                    </div>
                    <div className="w-20 text-center font-bold">{counter}</div>
                    <div className="w-5">
                      <FontAwesomeIcon
                        className="cursor-pointer text-gray-500"
                        onClick={() => {
                          updateCounter(1);
                        }}
                        icon={faPlus}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="w-full mt-4 sm:mt-0 flex">
                <div className="lg:w-2/3 flex flex-wrap text-sm">
                  <div className="flex w-32 mt-2">
                    <div className="rounded-full mt-1 w-5 h-5 bg-white shadow-inner shadow-lg"></div>
                    <div className="pl-2 pt-1">White</div>
                  </div>
                  <div className="flex w-32 mt-2">
                    <div className="rounded-full mt-1 w-5 h-5 bg-gray-700"></div>
                    <div className="pl-2 pt-1 text-gray-700">Space Gray</div>
                  </div>
                  <div className="flex w-32 mt-2">
                    <div className="rounded-full mt-1 w-5 h-5 bg-red-700"></div>
                    <div className="pl-2 pt-1 text-red-700">Red</div>
                  </div>
                  <div className="flex w-32 mt-2">
                    <div className="rounded-full mt-1 w-5 h-5 bg-black"></div>
                    <div className="pl-2 pt-1">Black</div>
                  </div>
                  <div className="flex w-32 mt-2">
                    <div className="rounded-full mt-1 w-5 h-5 bg-blue-800"></div>
                    <div className="pl-2 pt-1 text-blue-800">Blue</div>
                  </div>
                  <div className="flex w-32 mt-2">
                    <div className="rounded-full mt-1 w-5 h-5 bg-yellow-200"></div>
                    <div className="pl-2 pt-1 text-yellow-300">Gold</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="w-full text-center md:text-right md:flex-1 font-bold p-4">
              <NumberFormat
                value={totalPrice}
                displayType={"text"}
                thousandSeparator={true}
                prefix={"฿ "}
              />
            </div>
          </div>
          <div className="border-b mt-5 border-gray-400 pb-4">
            <div className="font-bold">Payment Information</div>
            <div className="text-gray-500 mt-1 text-sm">
              Choose your method of payment.
            </div>
            <div className="flex flex-wrap py-2">
              <div className="w-full md:flex-1 -my-18">
                <div className="max-w-lg">
                  <img
                    alt="card"
                    className="w-100 shadow-lg rounded-xl"
                    src="https://www.giraffee.co/wp-content/uploads/2020/05/Citi-Reward-Point-Credit-Card.jpg"
                  />
                </div>
              </div>
              <div className="w-full mt-8 md:mt-0 md:px-4 md:flex-1">
                <div className="flex -mt-4">
                  <div className="w-20 mr-2">
                    <img src={MasterCard} alt="card" />
                  </div>
                  <div className="w-20 mr-2">
                    <img src={Visa} alt="card" />
                  </div>
                </div>
                <div className="flex flex-wrap">
                  <div className="w-full md:flex-1 mt-4 px-2">
                    <div className="text-sm font-bold">Credit Card Number</div>
                    <div className="mt-1 w-full">
                      <NumberFormat
                        className="w-full border rounded-xl px-4 h-9"
                        placeholder="XXXX XXXX XXXX XXXX"
                        displayType={"Input"}
                        format="#### #### #### ####"
                      />
                    </div>
                  </div>
                  <div className="w-full md:flex-1 mt-4 px-2">
                    <div className="text-sm font-bold">Expiration Code</div>
                    <div className="mt-1 w-full">
                      <input
                        type="text"
                        className="w-full border rounded-xl px-4 h-9"
                      />
                    </div>
                  </div>
                </div>
                <div className="flex flex-wrap">
                  <div className="w-full md:flex-1 mt-4 px-2">
                    <div className="text-sm font-bold">Security Code</div>
                    <div className="mt-1 w-full">
                      <input
                        type="text"
                        className="w-full border rounded-xl px-4 h-9"
                      />
                    </div>
                  </div>
                  <div className="w-full md:flex-1 mb-2 mt-4 px-2">
                    <div className="text-sm font-bold">Postal Code</div>
                    <div className="mt-1 w-full">
                      <input
                        type="text"
                        className="w-full border rounded-xl px-4 h-9"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="border-b mt-5 border-gray-400 pb-4">
            <div className="flex font-bold">
              <div className="flex-1 text-left">Sub total</div>
              <div className="flex-1 text-right font-normal">
                <NumberFormat
                  value={totalPrice}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"฿ "}
                />
              </div>
            </div>
            <div className="flex mt-3 font-bold">
              <div className="flex-1 text-left">Shipping</div>
              <div className="flex-1 text-right font-normal">Free</div>
            </div>
            <div className="flex mt-3 font-bold">
              <div className="flex-1 text-left">Tax Include (7%)</div>
              <div className="flex-1 text-right font-normal">
                <NumberFormat
                  value={tax}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"฿ "}
                />
              </div>
            </div>
          </div>
          <div className="flex mt-4 font-bold border-b border-gray-400 pb-4">
            <div className="flex-1 text-left">TOTAL</div>
            <div className="flex-1 text-right">
              <NumberFormat
                value={totalPrice + tax}
                displayType={"text"}
                thousandSeparator={true}
                prefix={"฿ "}
              />
            </div>
          </div>
          <div className="flex mt-4 font-bold pb-4">
            <div className="md:w-4/6  text-left"></div>
            <div className="md:w-2/6 w-full text-right">
              <div
                onClick={clickBuy}
                className="rounded-lg cursor-pointer text-md text-center bg-gradient-to-r from-purple-400 to-blue-500 hover:from-pink-500 hover:to-orange-500 text-white font-semibold px-3 py-3 "
              >
                <FontAwesomeIcon icon={faShoppingCart} />
                <span className="ml-2 pr-4">Buy</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Summary;
