import React, { useState, useEffect } from "react";

const Search = props => {
  const [textSearch, setTextSearch] = useState("");
  const changeText = e => {
    setTextSearch(e.target.value);
  };

  const clickSearch = () => {
    const params = {
      search: textSearch
    };
    props.afterSearch(params);
  };

  useEffect(() => {
    clickSearch();
  }, [textSearch]);

  return (
    <div className="p-4 mt-1">
      <div className="border rounded-sm shadow-lg  border-gray-200">
        {/* <div className="border-b p-2 text-xs">Search Option</div> */}
        <div className="px-3 py-2">
          <div className="text-xs w-full font-bold mb-1">Search</div>
          <div className="pb-3 flex flex-wrap">
            <div className="w-full xl:w-2/3">
              <input
                type="text"
                className="rounded-sm border border-gray-300 p-1 pl-4 w-full h-9"
                onInput={changeText}
                placeholder="Enter your keyword"
              />
            </div>
            <div className="w-full mt-2 xl:mt-0 xl:w-1/3 xl:pl-2">
              <button
                onClick={clickSearch}
                type="button"
                className="text-sm w-full truncate ... bg-gradient-to-r from-purple-400 to-blue-500 hover:from-pink-500 hover:to-orange-500 text-white font-semibold px-2 h-9 py-1 rounded-sm xl:ml-1"
              >
                Search
              </button>
            </div>
          </div>
          <div className="mt-3 border-b font-bold border-gray-200 pb-2">
            <div className="text-xs">Brand</div>
            <div className="text-sm pt-1 font-normal">
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">Apple</span>
                </label>
              </div>
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">Samsumg</span>
                </label>
              </div>
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">Xaiomi</span>
                </label>
              </div>
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">Oppo</span>
                </label>
              </div>
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">Huawei</span>
                </label>
              </div>
            </div>
          </div>
          <div className="mt-3 border-b font-bold border-gray-200 pb-2">
            <div className="text-xs">Colors</div>
            <div className="text-sm pt-1 font-normal">
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">Black</span>
                </label>
              </div>
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">Blue</span>
                </label>
              </div>
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">Gold</span>
                </label>
              </div>
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">White</span>
                </label>
              </div>
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">Blue</span>
                </label>
              </div>
            </div>
          </div>
          <div className="mt-3 font-bold border-gray-200 pb-2">
            <div className="text-xs">Capacity</div>
            <div className="text-sm pt-1 font-normal">
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">64 GB</span>
                </label>
              </div>
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">128 BG</span>
                </label>
              </div>
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">256 BG</span>
                </label>
              </div>
              <div>
                <label className="inline-flex items-center mt-1">
                  <input
                    type="checkbox"
                    className="form-checkbox h-5 w-5 text-gray-600"
                  />
                  <span className="ml-2 text-gray-700">512 BG</span>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Search;
