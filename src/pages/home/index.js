import React, { useState } from "react";
import ItemCard from "./itemCard.js";
import Search from "./search.js";
import NavBar from "./../../shared/nav/navBar.js";
import { useHistory } from "react-router-dom";

import { MOBILES } from "./../../fakedb/mobile.js";
const Home = props => {
  const history = useHistory();
  const db = MOBILES;
  const [items, setItems] = useState(db);
  const clickPurchase = item => {
    history.push(`/summary/${item.id}`);
  };
  const search = data => {
    var txt = data.search;
    var result = db;
    if (txt) {
      result = db.filter(o => o.name.includes(txt));
    }
    setItems(result);
  };
  return (
    <>
      <NavBar />
      <div className="flex flex-wrap">
        <div className="w-full md:w-1/4">
          <Search afterSearch={search} />
        </div>
        <div className="w-full md:w-3/4 pr-3">
          <div className="flex flex-wrap overflow-hidden md:-mx-px lg:-mx-1 xl:-mx-3">
            {items.map((o, i) => {
              return (
                <div
                  key={i}
                  className="my-2 px-2 w-full overflow-hidden  md:my-px md:px-px sm:w-1/2 md:w-1/2 lg:my-1 lg:px-1 lg:w-1/3 xl:my-3 xl:px-3 xl:w-1/4"
                >
                  <ItemCard
                    afterClickPurchase={clickPurchase}
                    data={o}
                  ></ItemCard>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
};
export default Home;
