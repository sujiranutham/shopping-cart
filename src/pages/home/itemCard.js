import React from "react";
import NumberFormat from "react-number-format";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";

const ItemCard = props => {
  const data = props.data;
  const clickPurchase = () => {
    props.afterClickPurchase(data);
  };
  return (
    <div className="border-t border-gray-100 bg-white shadow-lg rounded-lg overflow-hidden mx-2">
      <img
        className="h-56 w-full h-full rounded-t-lg object-cover mt-2"
        src={data.image}
        alt={data.name}
      />
      <div className="px-4 py-2">
        <h1 className="text-gray-900 font-bold text-lg text-center mt-2 uppercase">
          {data.name}
        </h1>
        <p className="text-gray-600 text-sm mt-1">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Modi quos
          quidem sequi illum facere recusandae voluptatibus
        </p>
      </div>

      <div className="py-2 bg-gray-800">
        <h1 className="text-center text-white font-bold text-xl">
          <NumberFormat
            value={data.price}
            displayType={"text"}
            thousandSeparator={true}
            prefix={"฿ "}
          />
        </h1>
      </div>
      <div
        onClick={clickPurchase}
        className="cursor-pointer text-md text-center bg-gradient-to-r from-purple-400 to-blue-500 hover:from-pink-500 hover:to-orange-500 text-white font-semibold px-3 py-3  rounded-b-sm"
      >
        <FontAwesomeIcon icon={faShoppingCart} />
        <span className="ml-2 pr-4">Buy</span>
      </div>
    </div>
  );
};
export default ItemCard;
