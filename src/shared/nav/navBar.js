import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome, faCaretRight } from "@fortawesome/free-solid-svg-icons";
const NavBar = props => {
  return (
    <div className="fixed top-16 flex bg-white left-0 w-full shadow-lg py-3 px-6 text-sm">
      <div className="nav-bar font-bold">
        <FontAwesomeIcon icon={faHome} />
        <span className="ml-2">Shopping cart</span>
      </div>
      <div className="pl-4 pr-1 text-sm ">
        <FontAwesomeIcon icon={faCaretRight} />
      </div>
      <div className="nav-bar font-bold">
        <span className="ml-2">Product</span>
      </div>
      <div className="pl-4 pr-1 text-sm">
        <FontAwesomeIcon icon={faCaretRight} />
      </div>
      <div className="nav-bar">
        <span className="ml-2">Summary</span>
      </div>
    </div>
  );
};
export default NavBar;
