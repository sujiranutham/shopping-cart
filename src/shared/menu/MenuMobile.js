import React from "react";
import { Link } from "react-router-dom";
const MenuMobile = (props) => {
  const menus = props.listMenu;
  const showMenu = props.showMenu;
  const active = "bg-gray-900 ";
  return (
    <div className={`sm:hidden ${showMenu ? "" : "hidden"}`} id="mobile-menu">
      <div className="px-2 pt-2 pb-3 space-y-1">
        {menus.map((m, i) => {
          return (
            <Link
              className={`block px-3 text-white py-2 rounded-md text-base font-medium 
              ${i === 0 ? active : ""}`}
              key={i}
              to={m.path}
            >
              {m.name}
            </Link>
          );
        })}
      </div>
    </div>
  );
};
export default MenuMobile;
