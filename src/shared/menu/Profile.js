import React, { useState } from "react";
import { Link } from "react-router-dom";
import ProfileImage from "../../assets/images/profile-pic.jpg";
const Profile = props => {
  const [isShow] = useState(false);
  const btnProfileClass = [
    "bg-gray-800",
    "flex",
    "text-sm",
    "rounded-full",
    "focus:outline-none",
    "focus:ring-2",
    "focus:ring-offset-2",
    "focus:ring-offset-gray-800",
    "focus:ring-white"
  ];
  return (
    <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
      <div className="ml-3 relative">
        <div className="flex">
          <div className="mr-5 hidden md:inline text-white capitalize  font-bold text-lg">
            Ananchai S.
          </div>
          <button
            // onClick={() => setIsShow(!isShow)}
            type="button"
            className={btnProfileClass.join(" ")}
          >
            <img className="h-8 w-8 rounded-full" src={ProfileImage} alt="" />
          </button>
        </div>
        {isShow ? <ProfileItem /> : null}
      </div>
    </div>
  );
};

const ProfileItem = props => {
  const profileClass = [
    "origin-top-right",
    "absolute",
    "right-0",
    "mt-2",
    "w-48",
    "rounded-md",
    "shadow-lg",
    "py-1",
    "bg-white",
    "ring-1",
    "ring-black",
    "ring-opacity-5",
    "focus:outline-none"
  ];
  const linkClass = [
    "font-bold",
    "block",
    "px-4",
    "py-2",
    "text-sm",
    "text-gray-700",
    "hover:bg-gray-100"
  ];
  return (
    <div className={profileClass.join(" ")}>
      <Link to="/" className={linkClass.join(" ")}>
        Your Profile
      </Link>
    </div>
  );
};
export default Profile;
