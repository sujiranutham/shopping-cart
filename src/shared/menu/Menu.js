import React, { useState } from "react";
import Profile from "./Profile.js";
import MenuDesktop from "./MenuDesktop.js";
import MenuMobile from "./MenuMobile.js";

const IconClose = () => {
  return (
    <svg
      className="block h-6 w-6"
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
      aria-hidden="true"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M4 6h16M4 12h16M4 18h16"
      />
    </svg>
  );
};
const IconMenu = () => {
  return (
    <svg
      className=" h-6 w-6"
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
      aria-hidden="true"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M6 18L18 6M6 6l12 12"
      />
    </svg>
  );
};

const Menu = props => {
  const [showMenu, setShowMenu] = useState(false);
  const listMenu = [
    { name: "Product", path: "/home" },
    { name: "Page 404", path: "/test" }
  ];
  const clickMenu = () => {
    setShowMenu(!showMenu);
  };

  return (
    <div className="bg-gray-800 shadow-lg fixed top-0 left-0 w-full">
      <div className="px-4 px-2 ">
        <div className="relative flex items-center justify-between h-16">
          <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
            <button
              type="button"
              className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 "
              aria-controls="mobile-menu"
              aria-expanded="false"
              onClick={clickMenu}
            >
              <span className="sr-only">Open main menu</span>
              {!showMenu ? <IconClose /> : <IconMenu />}
            </button>
          </div>

          <MenuDesktop listMenu={listMenu}></MenuDesktop>
          <Profile></Profile>
        </div>
      </div>
      <MenuMobile showMenu={showMenu} listMenu={listMenu}></MenuMobile>
    </div>
  );
};

export default Menu;
