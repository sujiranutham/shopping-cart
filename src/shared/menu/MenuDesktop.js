import React from "react";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";

const MenuDesktop = props => {
  const history = useHistory();
  const menus = props.listMenu;
  const active = "bg-gray-900 ";
  const clickLogo = () => {
    history.push(`/home`);
  };
  return (
    <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
      <div className="flex-shrink-0 flex items-center">
        <img
          className="block lg:hidden h-8 w-auto"
          src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg"
          alt="Workflow"
        />
        <div
          onClick={clickLogo}
          className="hidden lg:block cursor-pointer  h-8 w-auto"
        >
          <img
            className="inline h-8 w-auto"
            src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg"
            alt="Workflow"
          />
          <span className="pl-4 pr-2 text-white font-bold text-lg">
            SHOPPING CART
          </span>
        </div>
      </div>
      <div className="hidden sm:block sm:ml-6">
        <div className="flex space-x-4">
          {menus.map((m, i) => {
            return (
              <Link
                className={`text-white px-3 py-2 rounded-md text-sm font-medium ${
                  i === 0 ? active : ""
                }`}
                key={i}
                to={m.path}
              >
                {m.name}
              </Link>
            );
          })}
        </div>
      </div>
    </div>
  );
};
export default MenuDesktop;
