import React from "react";
const EmptyLayout = props => {
  return (
    <div className="min-h-screen pb-20 relative">
      <div className="px-20 py-20">{props.children}</div>
      <div className="absolute text-gray-800 text-sm bottom-0 bg-gray-200 shadow-lg py-3 left-0 w-full text-center">
        Power by Ananchai P.
      </div>
    </div>
  );
};
export default EmptyLayout;
