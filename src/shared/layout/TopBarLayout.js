import React from "react";
import Menu from "../menu/Menu.js";

const TopBarLayout = props => {
  const { children } = props;
  return (
    <div className="min-h-screen pb-20 relative">
      <Menu></Menu>
      <div className="page-content pt-28">
        {React.cloneElement(children, { globalState: children.history })}
      </div>
      <div className="absolute text-gray-800 text-sm bottom-0 bg-gray-200 shadow-lg py-3 left-0 w-full text-center">
        Power by Ananchai P.
      </div>
    </div>
  );
};
export default TopBarLayout;
