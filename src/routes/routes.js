import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// route layout
import { TopBarLayoutRoute, EmptyLayoutRoute } from "./LayoutRoutes.js";
// page component
import Home from "./../pages/home";
import About from "./../pages/about";
import Summary from "./../pages/summary";
import NotFound404 from "./../pages/notfond/NotFound404";
const Routes = props => {
  return (
    <Router>
      <Switch>
        <TopBarLayoutRoute exact path="/" component={Home}></TopBarLayoutRoute>
        <TopBarLayoutRoute exact path="/home" component={Home} />
        <TopBarLayoutRoute exact path="/summary/:id" component={Summary} />
        <EmptyLayoutRoute exact path="/about" component={About} />
        <Route path="*" exact={true} component={NotFound404} />
      </Switch>
    </Router>
  );
};
export default Routes;
