import React from "react";
import { Route } from "react-router-dom";
import TopBarLayout from "./../shared/layout/TopBarLayout.js";
import EmptyLayout from "./../shared/layout/EmptyLayout.js";

const TopBarLayoutRoute = ({ component: Component, ...rest }) => {
  const render = props => {
    return (
      <TopBarLayout>
        <Component {...props} />
      </TopBarLayout>
    );
  };
  return <Route {...rest} render={render} />;
};

const EmptyLayoutRoute = ({ component: Component, ...rest }) => {
  const render = props => {
    return (
      <EmptyLayout>
        <Component {...props} />
      </EmptyLayout>
    );
  };
  return <Route {...rest} render={render} />;
};
export { TopBarLayoutRoute, EmptyLayoutRoute };
